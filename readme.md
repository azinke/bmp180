# BMP180

The BMP180 is a high precison digital pressure sensors adapted for many applications such as vertical velocity indication, weather forecast, etc. It offers I²C interface for configuration and data retrieval.

This library is intended to simplify the usage of the sensor. With accuracy in mind, this library is build around the kalman filter for state estimation.

## key features

Features | Descriptions
:-------|:------------
Pressure range | 300 ... 1100hPa (+9000m ... -500m relating to sea level)
Supply voltage | 1.8 ... 3.6V (VDD) - 1.62V ... 3.6V (VDDio)
Low power | 5µA at 1 sample/second in standard mode
Low noise | 0.06hPa (0.5m) in ultra low power mode - 0.02hPa (0.17m) advanced resolution mode
Interface | I²C


## How to use this library

The usage of the library is pretty straightforward. The table below show up a summary of the available methods.

As the pressure data varies quite a lot, a complementary filter is built-within the library. The default value of the filter's parameter is `0.9`. Then, new data are less trusted.

Default pressure oversampling parameter is set to `0`.

Methods | Descriptions
:-------|:------------
`void begin(bool enable_filter)` | read calibration data and configure the internal filter of the library.
`bool reset()` | performe a software reset of the sensor
`long getTemp()` | read and compute the temperature measurement
`long getPressure()` | read and compute the pressure measurement
`float getAltitude()` | compute the altitude out of the pressure measurement based on sea level pressure.
`float getRelativeAltitude()` | compute the altitude based on a reference pressure
` void setReferencePressure()` | set the reference pressure to compute the relative altitude
`long getReferencePressure()` | read out the reference pressure
`long getSeaLevelPressure()` | read out the sea level pressure which is used to compute absolute altitude
`long getRawTemp()` | get the raw temperature data out of the sensor
`long getRawPressure()` | get the raw pressure data out of the sensor
`bool configure(uint8_t oss)` | set the oversampling parameter for pressure measurement. The `oss` parameter can take any value between `0` and `3`.
`bool isConnected()` | check out wiring error of the sensor. Return true when well done.
`bool enableFilter(bool enable)` | enable or disable the complementary filter built-in the library
`void setAlpha(float alpha)` | set the complementary filter parameter.


