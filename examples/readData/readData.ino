#include <bmp180.h>

BMP180 bmp;

void setup(){
	Serial.begin(9600);
	Serial.println("BMP180 - Digital pressure sensor");
	bmp.begin(true);
	while(!bmp.isConnected()){
		Serial.println("sensor not found !");
	}
	Serial.println("Connected !");
}

void loop(){
	Serial.print("Temperature (raw): ");
	Serial.print(bmp.getTemp());
	Serial.println(" °C");
	
	Serial.print("Pressure (raw): ");
	Serial.print(bmp.getPressure());
	Serial.println(" Pa");
	Serial.println();	
	delay(200);
}
