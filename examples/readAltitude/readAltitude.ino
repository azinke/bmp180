#include <bmp180.h>

BMP180 bmp;

void setup(){
	Serial.begin(9600);
	bmp.begin(true);
}

void loop(){
	Serial.print("Pressure: ");
	Serial.print(bmp.getPressure());
	Serial.println(" Pa");

	Serial.print("Reference pressure: ");
	Serial.print(bmp.getReferencePressure());
	Serial.println(" Pa");

	Serial.print("Absolute altitude: ");
	Serial.print(bmp.getAltitude(), 3);
	Serial.println(" m");

	Serial.print("Relative altitude: ");
	Serial.print(bmp.getRelativeAltitude(),3);
	Serial.println(" m");
	
	Serial.println();

	delay(500);
}
