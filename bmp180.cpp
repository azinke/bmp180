/**
 *	@author: AMOUSSOU Z. Kenneth
 *	@date: 12-11-2018
 *	@version: 1.0
 *
 *	@external-library:
 *		Wire: I2C communication library
 * */
#include "bmp180.h"

/**
 *	@constructor: BMP180
 */
BMP180::BMP180(){
	// TODO: Check out what could be done during instantiation
	
	#if BMP_DEBUG == 0x01
	    Serial.println("Init ...");
	#endif
}

/**	
 *	@function: _read
 *	@summary: read a single byte from Two Wire Interface (TWI)
 *	@parameters:
 *		addr: sensor's I2C address
 *	@return:
 *		uint8_t: byte read from the sensor
 */
uint8_t BMP180::_read(uint8_t addr){
	Wire.beginTransmission(ADDR);		// use the 7-bits address
	Wire.write(addr);
	Wire.endTransmission(false);
#ifdef __STM32F1__
    Wire.requestFrom(ADDR, 1);
#else
	Wire.requestFrom(ADDR, 1, true);
#endif
	return Wire.read();
}

/**	
 *	@function: _write
 *	@summary: write a single byte on the Two Wire Interface (TWI)
 *	@parameters:
 *		addr: sensor's I2C address
 *		data: data (byte) to write
 *	@return: none
 */
void BMP180::_write(uint8_t addr, uint8_t data){
	Wire.beginTransmission(ADDR);
	Wire.write(addr);
	Wire.write(data);
	Wire.endTransmission(true);
}

/**
 *	@function: begin
 *	@summary: initialization of the sensor
 *	@parameters:
 *		enable_filter: enable or disable internal filter
 *					   complementary filter is the default one
 *	@return: none
 */
void BMP180::begin(bool enable_filter = true){
    Wire.setClock(400000);
    Wire.begin();
	configure(3);			// oss = 3
	//_oss = 0;	
	_readParameters();		// read calibration data
    // read temperature
	getTemp();
	// init timer
	_timer = millis();
	// read initial pressure while filter is disabled
	_enable_filter = false;
	_ref_pressure = getPressure();
	_pressure = _ref_pressure;
	// configure filter
	_enable_filter = enable_filter;
	_alpha = 0.9;
	
}

/**
 *	@function: _readParameters
 *	@summary: read calibration data from the sensor's eeprom
 *	@parameters: none
 *	@return: none
 */
void BMP180::_readParameters(){
	_param.ac1 = (short)((uint16_t)_read(AC1) << 8 | _read(AC1 + 1));
	_param.ac2 = (short)((uint16_t)_read(AC2) << 8 | _read(AC2 + 1));
	_param.ac3 = (short)((uint16_t)_read(AC3) << 8 | _read(AC3 + 1));
	_param.ac4 = (unsigned short)((uint16_t)_read(AC4) << 8 | _read(AC4 + 1));
	_param.ac5 = (unsigned short)((uint16_t)_read(AC5) << 8 | _read(AC5 + 1));
	_param.ac6 = (unsigned short)((uint16_t)_read(AC6) << 8 | _read(AC6 + 1));
	_param.b1 = (short)((uint16_t)_read(B1) << 8 | _read(B1 + 1));
	_param.b2 = (short)((uint16_t)_read(B2) << 8 | _read(B2 + 1));
	_param.mb = (short)((uint16_t)_read(MB) << 8 | _read(MB + 1));
	_param.mc = (short)((uint16_t)_read(MC) << 8 | _read(MC + 1));
	_param.md = (short)((uint16_t)_read(MD) << 8 | _read(MD + 1));
	
	#if BMP_DEBUG == 0x01
	// Datasheet data
	_param.ac1 = 408;
	_param.ac2 = -72;
	_param.ac3 = -14383;
	_param.ac4 = 32741;
	_param.ac5 = 32757;
	_param.ac6 = 23153;
	_param.b1 = 6190;
	_param.b2 = 4;
	_param.mb = -32768;
	_param.mc = -8711;
	_param.md = 2868;	
	// 

    Serial.print("AC1: ");
    Serial.println(_param.ac1);
    
    Serial.print("AC2: ");
    Serial.println(_param.ac2);
	    
    Serial.print("AC3: ");
    Serial.println(_param.ac3);
    
    Serial.print("AC4: ");
    Serial.println(_param.ac4);
	    
    Serial.print("AC5: ");
    Serial.println(_param.ac5);
	    
	Serial.print("AC6: ");
    Serial.println(_param.ac6);
	    
	Serial.print("B1: ");
    Serial.println(_param.b1);

    Serial.print("B2: ");
    Serial.println(_param.b2);
		
    Serial.print("MB: ");
    Serial.println(_param.mb);

    Serial.print("MC: ");
    Serial.println(_param.mc);

    Serial.print("MD: ");
    Serial.println(_param.md);
	#endif
	return;
}

/**
 *	@function: isConnected
 *	@summary: check the wiring error of the sensor
 *	@parameters: none
 *	@return:
 *		bool: true when well wired
 */
bool BMP180::isConnected(){
	if(_read(ID) == WHOAMI){
	    #if BMP_DEBUG == 0x01
	        Serial.println("Connected! ...");
	    #endif
	    return true;
    }
	else return false;
}


/**
 *	@function: reset
 *	@summary: software reset of the sensor
 *	@parameters: none
 *	@return:
 *		bool: true when reset performed with success
 */
bool BMP180::reset(){
	_write(SOFT_RESET, 0xB6);
	return true;
}


/**
 *	@function: getParameters
 *	@summary: get out the calibration
 *	@parameters: none
 *	@return:
 *		PARAMETERS: calibration data
 */
PARAMETERS BMP180::getParameters(){
	return _param;
}

/**
 *	@function: getRawTemp
 *	@summary: get out raw temperature data
 *	@parameters: none
 *	@return:
 *		long: raw temperature data
 */
long BMP180::getRawTemp(){
	// u_temp: uncompensated temperature
	long u_temp = 0;
	_write(CTRL_MEAS, 0x2E);
	delay(10);
	u_temp = ((uint16_t)_read(OUT_MSB) << 8) + (uint16_t)_read(OUT_LSB);
	return u_temp;
}

/**
 *	@function: getRawPressure
 *	@summary: get out raw pressure data
 *	@parameters: none
 *	@return:
 *		long: raw pressure data
 */
long BMP180::getRawPressure(){	
	// u_pressure: uncompensated pressure
	int32_t u_pressure = 0;
	_write(CTRL_MEAS, 0x34 | (_oss << 6));
	if(_oss == 0) delay(5);
	else if(_oss == 1) delay(30);
	else if(_oss == 2) delay(50);
	else delay(100);
	u_pressure = ((uint32_t)_read(OUT_MSB) << 16 | \
	              (uint32_t)_read(OUT_LSB) << 8 | \
	              (uint32_t)_read(OUT_XLSB)) >> (8 - _oss);
	return (long)u_pressure;
}

/**
 *	@function: getTemp
 *	@summary: read and compute the temperature data
 *		Temperature computation procedure:
 *			UT: uncompensated temperature
 *			X1 = (UT - AC6) * AC5 / 2^15
 *			X2 = (MC * 2^11)/(X1 + MD)
 *			B5 = X1 + X2
 *			T = (B5 + 8) / 2^4 (temperature in 0.1°C)
 *			T = T / 10 (temperature in °C)
 *	@constants:
 *		2^15 = 32768
 *		2^11 = 2048
 *		2^4	 = 16
 *	@parameters: none
 *	@return:
 *		float: computed temperature
 */
float BMP180::getTemp(){
	long u_temp = getRawTemp();	// read raw uncompensated temp. data
	#if BMP_DEBUG == 0x01
	u_temp = 27898;
	#endif
	
	_param.x1 = (long)((u_temp - _param.ac6) * _param.ac5) >> 15;
	_param.x2 = ((long)_param.mc << 11)/(_param.x1 + _param.md);
	_param.b5 = (long)(_param.x1 + _param.x2);
	#if BMP_DEBUG == 0x01
	Serial.print("X1: ");
	Serial.println(_param.x1);
	Serial.print("X2: ");
	Serial.println(_param.x2);
	Serial.print("B5: ");
	Serial.println(_param.b5);
	#endif	
	_temp =(long) (_param.b5 + 8) >> 4;	// temperature in 0.1 °C
	return _temp/10;
}

/**
 *	@function: getPressure
 *	@summary: read and compute the pressure
 *		Pressure computation procedure:
 *			UP: uncompensated pressure
 *			B6 = B5 - 4000
 *			X1 = (B2 * (B6*B6/2^12))/2^11
 *			X2 = AC2*B6/2^11
 *			X3 = X1 + X2
 *			B3 = (((AC1*4 + X3) << oss) + 2)/4
 *			X1 = AC3*B6/2^13
 *			X2 = (B1*(B6*B6/2^12))/2^3
 *			X3 = ((X1 + X2) + 2)/2^2
 *			B4 = AC4*(unsigned long)(X3 + 32768)/2^15
 *			B7 = ((unsigned long)UP - B3) * (50000 >> oss)
 *			if(B7 < 0x800000000){ p = (B7*2)/B4 }
 *			else { p = (B7/B4) * 2 }
 *			X1 = (p /2^8)*(p/2^8)
 *			X1 = (X1*3038)/2^16
 *			X2 = (-7357*p)/2^16
 *			p = p + (X1 + X2 + 3791)/2^4
 *	@constants:
 *		2^16 = 65536
 *		2^15 = 32768
 *		2^13 = 8192
 *		2^12 = 4096
 *		2^11 = 2048
 *		2^8 = 256
 *		2^4 = 16
 *		2^2 = 4
 *	@parameters: none
 *	@return:
 *		float: computed pressure
 */
long BMP180::getPressure(){
	long pressure;
	// read uncompensated pressure data
    // read five times raw pressure and then compute  the mean
	long u_pressure = getRawPressure(); 
	// Calling this method update B5 with current temperature data
	// Then the pressure can be get independantly of the temperature
	// (without reading temperature in advance)
	if(millis() - _timer >= 1000){ getTemp(); _timer = millis(); }
	#if BMP_DEBUG == 0x01
		u_pressure = 23843;
	#endif
	_param.b6 = _param.b5 - 4000;
	#if BMP_DEBUG == 0x01
	Serial.print("B6: ");
	Serial.println(_param.b6);
	#endif	
	_param.x1 = (_param.b2 * ((_param.b6 * _param.b6) >> 12)) >> 11;
	#if BMP_DEBUG == 0x01
	Serial.print("X1: ");
	Serial.println(_param.x1);
	#endif	
	_param.x2 = (_param.ac2 * _param.b6) >> 11;
	#if BMP_DEBUG == 0x01
	Serial.print("X2: ");
	Serial.println(_param.x2);
	#endif	
	_param.x3 = _param.x1 + _param.x2;
	#if BMP_DEBUG == 0x01
	Serial.print("X3: ");
	Serial.println(_param.x3);
	#endif	
	_param.b3 = (((_param.ac1 * 4 + _param.x3) << _oss) + 2) >> 2;
	#if BMP_DEBUG == 0x01
	Serial.print("B3: ");
	Serial.println(_param.b3);
	#endif	
	_param.x1 = (_param.ac3 * _param.b6) >> 13;
	#if BMP_DEBUG == 0x01
	Serial.print("X1: ");
	Serial.println(_param.x1);
	#endif	
	_param.x2 = (_param.b1*((_param.b6*_param.b6) >> 12)) >> 16;
	#if BMP_DEBUG == 0x01
	Serial.print("X2: ");
	Serial.println(_param.x2);
	#endif	
	_param.x3 = ((_param.x1+_param.x2) + 2) >> 2;
	#if BMP_DEBUG == 0x01
	Serial.print("X3: ");
	Serial.println(_param.x3);
	#endif	
	_param.b4 = (_param.ac4 * (unsigned long)(_param.x3 + 32768)) >> 15;
	#if BMP_DEBUG == 0x01
	Serial.print("B4: ");
	Serial.println(_param.b4);
	#endif	
	_param.b7 = ((unsigned long)u_pressure - _param.b3)*(50000 >> _oss);
	#if BMP_DEBUG == 0x01
	Serial.print("B7: ");
	Serial.println(_param.b7);
	#endif	
	if(_param.b7 < 0x80000000) pressure = (_param.b7 << 1)/_param.b4;
	else pressure = (_param.b7/_param.b4) << 1;
	_param.x1 = (pressure >> 8) * (pressure >> 8);
	#if BMP_DEBUG == 0x01
	Serial.print("X1: ");
	Serial.println(_param.x1);
	#endif	
	_param.x1 = (_param.x1 * 3038) >> 16;
	#if BMP_DEBUG == 0x01
	Serial.print("X1: ");
	Serial.println(_param.x1);
	#endif	
	_param.x2 = (-7357 * pressure) >> 16;
	#if BMP_DEBUG == 0x01
	Serial.print("X2: ");
	Serial.println(_param.x2);
	#endif	
	pressure = pressure + ((_param.x1 + _param.x2 + 3791) >> 4);
	
	// Filtering pressure data
	// pressure: new computed data
	// _pressure: pressure previously computed
	if(_enable_filter){
		// low pass filter
		_pressure = _alpha * _pressure + (1 - _alpha) * pressure;
	}else _pressure = pressure;	
	return _pressure;		// pressure in Pa
}

/**
 *	@function: getAltitude
 *	@summary: read and compute the altitude out of the pressure
 *			  with reference pressure set as sea level pressure
 *	@parameters: none
 *	@return:
 *		float: computed temperature
 */
float BMP180::getAltitude(){
	float pressure = getPressure();
	// altitude in meter
	return 44330.0 * (1.0 - pow((float)(pressure/P0), 1.0/5.255));
}

/**
 *	@function: getRelativeAltitude
 *	@summary: read and compute the relative altitude out of the pressure
 *	@parameters: none
 *	@return:
 *		float: computed temperature
 */
float BMP180::getRelativeAltitude(){
	float pressure = getPressure();
	// altitude in meter
	return 44330.0 * (1.0 - pow((float)(pressure/_ref_pressure), 1.0/5.255));
}

/**
 *	@function: setReferencePressure
 *	@summary: set the pressure reference for altitude computation
 *	@parameters:
 *		pressure: pressure to set as reference
 *	@return: none
 */
void BMP180::setReferencePressure(long pressure){
	_ref_pressure = pressure;
}

/**
 *	@function: getPressureReference
 *	@summary: get the reference pressure
 *	@parameters:
 *		pressure: pressure to set as reference
 *	@return: none
 */
long BMP180::getReferencePressure(){
	return _ref_pressure;
}


/**
 *	@function: configure
 *	@summary: sensor configuration
 *			- set sensor's pressure oversampling config
 *	@parameters:
 *		oss: oversamping parameter (for pressure)
 *	@return:
 *		bool: true when configuration done with success
 */
bool BMP180::configure(uint8_t oss){
    if(oss > 3) return false;
    _oss = oss;
//	_buffer = _read(CTRL_MEAS);
//	_buffer = (_buffer & (0x3F)) | _oss;
//	_write(CTRL_MEAS, _buffer);
	return true;
}

/**
 *	@function: enableFilter
 *	@summary: enable or disable the built-in complementary filter
 *	@parameters:
 *		enable: true | false to enable or disable the complementary filter built-in
 *	@return:
 *		bool: true after good execution
 */
bool BMP180::enableFilter(bool enable){
	_enable_filter = enable;
	return true;
}

/**
 *	@function: setAlpha
 *	@summary: update the complementary filter parameter
 *	@parameters:
 *		alpha: new filter parameter
 *	@return: none
 */
void BMP180::setAlpha(float alpha){
	_alpha = alpha;
	return;
}
