/**
 *	@author: AMOUSSOU Z. Kenneth
 *	@date: 12-11-2018
 *	@version: 1.0
 *
 *	@external-library:
 *		Wire: I2C communication library
 **/
#ifndef H_BMP180
#define H_BMP180

#include <Wire.h>

#if ARDUINO < 100
#include <WProgram.h>
#else
#include <Arduino.h>
#endif

#define BMP_DEBUG   0x00
#define WHOAMI      0x55

// device 7-bit address
#define ADDR 0x77

// eeprom starting address
// only MSB address are defined
#define AC1 0xAA
#define AC2 0xAC
#define AC3 0xAE
#define AC4 0xB0
#define AC5 0xB2
#define AC6 0xB4
#define B1 0xB6
#define B2 0xB8
#define MB 0xBA
#define MC 0xBC
#define MD 0xBE

// Device registers address
#define ID 0xD0
#define CTRL_MEAS 0xF4
#define SOFT_RESET 0xE0
#define OUT_MSB 0xF6
#define OUT_LSB 0xF7
#define OUT_XLSB 0xF8

// sea level pressure
#define P0 101325

// Calibration parameters
typedef struct {
	short ac1;
	short ac2;
	short ac3;
	unsigned short ac4;
	unsigned short ac5;
	unsigned short ac6;
	short b1;
	short b2;
	long b3;
	unsigned long b4;
	long b5;
	long b6;
	unsigned long b7;
	long x1;
	long x2;
	long x3;
	short mb;
	short mc;
	short md;
} PARAMETERS;

class BMP180{
	public:
		BMP180();
		void begin(bool enable_filter);			// read calibration data out of the module's eeprom
		float getTemp();			// read temperature
		long getPressure();		// read pressure in hPa
		float getAltitude();	// get out altitude from the sensor
		float getRelativeAltitude();
		PARAMETERS getParameters();
		bool reset();			// perform a software reset
		
		void setReferencePressure(long pressure);		// set pressure reference for altitude computation
		long getReferencePressure();		//get out the reference pressure

		long getRawTemp();		// get out raw temperature data
		long getRawPressure();	// get out raw pressure data

		long getSeaLevelPressure();	// get out sea level pressure
		
		// configure oversampling ratio of the pressure measurement
		bool configure(uint8_t oss);
		bool isBusy();			// check if the sensor is performing a conversion
		bool isConnected();		// check if the sensor is well wired
		
		// Filter
		bool enableFilter(bool enable);
		void setAlpha(float alpha);

	private:
		uint8_t _buffer;		// the definition of this reserve some space in memory for the buffer
		long _temp;			// temperature
		long _pressure;		// pressure
		long _ref_pressure;	// reference pressure
		short _oss;			// pressure oversampling parameter
		uint32_t _timer;    // timer to get new temperature data to compute pressure
		PARAMETERS _param;		// calibration paramters of the sensor
		
		/* Filter coonfiguration */
		bool _enable_filter;
		float _alpha;

		/* private methods */
		uint8_t _read(uint8_t addr);					// read a byte from the sensor
		void _readBytes(uint8_t addr, uint8_t *buffer, uint8_t size);
		void _write(uint8_t addr, uint8_t data);		// write a byte on the sensor

		void _readParameters();	// read calibration's parameters from the sensor's eeprom
		
};

#endif
